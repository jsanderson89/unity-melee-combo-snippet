﻿using UnityEngine;
using System.Collections;

public class ComboScript : MonoBehaviour
{
	#region Variables
	#region Public Variables
	public Animator animator;
	#endregion

	#region Internal Variables
	enum State { IDLE, WALK, GROUND_ATTACK };
	enum AttackState { ATKONE, ATKTWO, ATKTHREE };

	State currentState;
	AttackState currentAttackState;
	#endregion
	#endregion

	#region Unity Lifecycle
	void Start () 
	{
		currentState = State.IDLE;
	}

	void Update () 
	{

		switch (currentState)
		{
			case State.IDLE:
				State_Idle();
				break;

			case State.GROUND_ATTACK:
				State_GroundAttack();
				break;

			default:
				break;
		}

	}
	#endregion

	#region States
	void State_Idle()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			currentState = State.GROUND_ATTACK;
			Enter_Attack_AttackOne();
		}
	}

	void State_GroundAttack()
	{
		switch (currentAttackState)
		{
			case AttackState.ATKONE:
				Attack_AttackOne();
				break;

			case AttackState.ATKTWO:
				Attack_AttackTwo();
				break;

			case AttackState.ATKTHREE:
				Attack_AttackThree();
				break;

			default:
				Exit_Attack();
				break;
		}
		
	}
	#endregion

	#region Attacks
	#region Enter Attack Animations
	void Enter_Attack_AttackOne()
	{
		currentAttackState = AttackState.ATKONE;
		animator.Play("Punch1");
	}

	void Enter_Attack_AttackTwo()
	{
		currentAttackState = AttackState.ATKTWO;
		animator.Play("Punch2");
	}

	void Enter_Attack_AttackThree()
	{
		Debug.Log("Enter_Attack_AttackThree");
		currentAttackState = AttackState.ATKTHREE;
		animator.Play("Punch3");
	}
	#endregion

	#region Exit Attack Animations
	void Exit_Attack()
	{
		Debug.Log("Exit Attack");
		currentState = State.IDLE;
		currentAttackState = AttackState.ATKONE;
	}
	#endregion

	#region Play Attack Animations
	void Attack_AttackOne()
	{
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
		Debug.Log("Attack1");

		if (stateInfo.IsName("Punch1"))
		{
			if (stateInfo.normalizedTime >= 1)
			{
				Exit_Attack();
			}

			if (Input.GetKeyDown(KeyCode.E))
			{
				Enter_Attack_AttackTwo();
			}
		}

		else 
		{
			Exit_Attack();
		}
	}

	void Attack_AttackTwo()
	{
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
		Debug.Log("Attack2");

		if (stateInfo.IsName("Punch2"))
		{
			if (stateInfo.normalizedTime >= 1)
			{
				Exit_Attack();
			}

			if (Input.GetKeyDown(KeyCode.E))
			{
				Enter_Attack_AttackThree();
			}
		}

		else
		{
			Exit_Attack();
		}
	}

	void Attack_AttackThree()
	{
		Debug.Log("Attack3");

		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

		if (stateInfo.IsName("Punch3"))
		{
			if (stateInfo.normalizedTime >= 1)
			{
				Exit_Attack();
			}
		}

		else
		{
			Debug.Log("Exiting Attack3");
			Exit_Attack();
		}
	}
	#endregion
	#endregion
}
